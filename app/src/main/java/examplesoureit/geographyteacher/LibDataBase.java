package examplesoureit.geographyteacher;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {User.class}, version = 1)
public abstract class LibDataBase extends RoomDatabase {
    public abstract UserDAO getUserDao();
}
