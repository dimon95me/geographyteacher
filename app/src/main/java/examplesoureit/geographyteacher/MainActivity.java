package examplesoureit.geographyteacher;

import android.arch.persistence.room.Room;
import android.database.sqlite.SQLiteConstraintException;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rx.Observable;
import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "my_tag";

    List<Country> countryList = new ArrayList<>();
    User user;

    @BindView(R.id.main_login)
    EditText loginEditText;

    @BindView(R.id.main_password)
    EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        retrofitGetData();


    }

    @OnClick(R.id.main_action_submit)
    void onSubmitclick(){
        boolean loginOk = false;

        final LibDataBase db = Room.databaseBuilder(getApplicationContext(), LibDataBase.class, "user-database").build();

        if (loginEditText.getText().toString().equals("")||passwordEditText.getText().toString().equals("")){
            Toast.makeText(this, "Write login and password", Toast.LENGTH_SHORT).show();
        } else {

            Observable.just(db)
                    .observeOn(Schedulers.io())
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .map(new Func1<LibDataBase, User>() {
                        @Override
                        public User call(LibDataBase libDataBase) {
                            User tempUser;

                            tempUser = db.getUserDao().getVerifyUser(loginEditText.getText().toString(), passwordEditText.getText().toString());
                            if (tempUser == null) return null;
                            return tempUser;
                        }
                    })
                    .subscribe(user-> {
                            if (user == null) {
                                Log.d(TAG, "onSubmitclick: have no user with thislogin and password");
                            } else {
                                Log.d(TAG, "onSubmitclick: login and password are right");

                                Inflater inflater = getLayoutInflater().in
                            }
                        }
                    );

        }
    }

    private void retrofitGetData() {
        Retrofit.getCountries(new Callback<List<Country>>() {
            @Override
            public void success(List<Country> countries, Response response) {
                MainActivity.this.countryList = countries;
                Toast.makeText(MainActivity.this, " Retrofit Countries length: " + countries.size(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, " Retrofit Countries length: " + countries.size());
                Toast.makeText(MainActivity.this, "Main countryList length: " + countryList.size(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Main countryList length: " + countryList.size());
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "failure: " + error.getKind());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (item.getItemId() == id) {
            onRegisterClick();
        }

        return true;
    }

    private void onRegisterClick() {
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View dialogView = layoutInflater.inflate(R.layout.register_dialog, null);
        final EditText name = dialogView.findViewById(R.id.register_name);
        final EditText login = dialogView.findViewById(R.id.register_login);
        final EditText pass = dialogView.findViewById(R.id.register_password);
        final EditText rePass = dialogView.findViewById(R.id.register_repassword);

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Enter register values")
                .setView(dialogView);

        builder.setPositiveButton("Ok", (i, k) -> {
            addNewUser(name, login, pass, rePass);
        });
        builder.setNegativeButton("Cancel", (i, k) ->
                Toast.makeText(MainActivity.this, "Cancel", Toast.LENGTH_SHORT).show());

        AlertDialog alertDialog = builder.create();
        alertDialog.show();


    }

    private void addNewUser(EditText name, EditText login, EditText pass, EditText rePass) {
        Log.d(TAG, "addNewUser: work");
        if (name.getText().toString() != null                                //Верификация полей ввода
                && login.getText().toString() != null
                && pass.getText().toString() != null
                && rePass.getText().toString() != null) {
            if (login.getText().toString().length() > 5) {
                if (pass.getText().toString().equals(rePass.getText().toString())) {
                    if (pass.getText().toString().length() > 5) {


                        Log.d(TAG, "onRegisterClick: all OK");
                        user = new User(name.getText().toString(),
                                login.getText().toString(),
                                pass.getText().toString());

                        Log.d(TAG, "addNewUser: " + user.toString());

                        final LibDataBase db = Room.databaseBuilder(getApplicationContext(), LibDataBase.class, "user-database").build();

                        Observable.just(db)
                                .observeOn(Schedulers.io())
                                .subscribeOn(AndroidSchedulers.mainThread())
                                .map(new Func1<LibDataBase, User>() {
                                    @Override
                                    public User call(LibDataBase libDataBase) {
                                        User tempUser;
                                        try {
                                            db.getUserDao().insertAll(user);
                                            tempUser = db.getUserDao().getLoginUser("Olivian");
                                        } catch (SQLiteConstraintException e) {
                                            Log.d(TAG, "call: We are have user with this login");
                                            tempUser = null;
                                        }

                                        return tempUser;
                                    }
                                })
                                .subscribe(new Action1<User>() {
                                    @Override
                                    public void call(User user) {
                                        if (user==null) {
//                                            Toast.makeText(MainActivity.this, "user poop", Toast.LENGTH_SHORT).show();
                                            Log.d(TAG, "call: user poop");
                                        } else {
                                            Log.d(TAG, "call: " + user.toString());
                                        }
                                    }
                                });


                    } else
                        Toast.makeText(MainActivity.this, "Password too short", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(MainActivity.this, "Passwords not equals", Toast.LENGTH_SHORT).show();
            } else Toast.makeText(MainActivity.this, "Too small login", Toast.LENGTH_SHORT).show();
        } else Toast.makeText(MainActivity.this, "fill alll fields", Toast.LENGTH_SHORT).show();
    }
}
