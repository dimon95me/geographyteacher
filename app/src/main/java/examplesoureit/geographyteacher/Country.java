package examplesoureit.geographyteacher;

import com.google.gson.annotations.SerializedName;

public class Country {
    @SerializedName("name")
    public String name;
    @SerializedName("capital")
    public String capital;
}
