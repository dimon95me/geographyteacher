package examplesoureit.geographyteacher;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;

public class Retrofit {

    private static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public interface ApiInterface{
        @GET("/v2/all")
        void getCountries(Callback<List<Country>> callback);
    }

    public static void getCountries(Callback<List<Country>> callback){
        apiInterface.getCountries(callback);
    }

}
