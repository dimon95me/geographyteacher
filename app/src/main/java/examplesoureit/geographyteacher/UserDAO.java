package examplesoureit.geographyteacher;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import rx.Single;

@Dao
public interface UserDAO {

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertAll(User... users);

    @Insert
    void insert(User user);

    @Delete
    void delete(User user);

    @Query("SElECT * from user WHERE login like :login")
    User getLoginUser(String login);

    @Query("SElECT * from user WHERE login like :login AND password like :password")
    User getVerifyUser(String login, String password);

    @Query("SELECT * from user")
    List<User> getAllUsers();

    @Query("DELETE FROM user")
    void deleteAll();

}
